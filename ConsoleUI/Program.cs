﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleUI
{
    class Program
    {
        private const int TABEL_DIMENSION_X = 10;
        private const int TABEL_DIMENSION_Y = 10;
        static void Main()
        {
            for (int i = 1; i <= TABEL_DIMENSION_Y; i++)
            {
                Tabel tabel = new Tabel(i, TABEL_DIMENSION_Y);
                tabel.VisTabel();
            }

            Console.ReadKey();
        }
    }
}
