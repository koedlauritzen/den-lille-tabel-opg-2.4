﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleUI
{
    class Tabel
    {
        private int _tabelX { get; set; }
        private int _tabelY { get; set; }
        public Tabel(int tabelX, int tabelY)
        {
            _tabelX = tabelX;
            _tabelY = tabelY;
        }

        public void VisTabel()
        {
            for (int i = 1; i <= _tabelY; i++)
            {
                Console.Write($"{ i * _tabelX } ");
                
            }
            Console.WriteLine("\n");
        }
    }
}
